<?php 
include('include/include.php') ;

//titre de la page
  echo '<h1>' . $titre_du_site . '</h1>' 
        . '<h3>Du ' . $_SESSION['periode'][0]->format('j/m/Y') .' Au '  .  $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;  
echo "<h2>Balance</h2>" ;

//variable pour la ligne des totaux
$total_debit=0;
$total_credit=0;
$total_solde_debit=0 ;
$total_solde_credit=0 ;

//on liste les comptes
$listecompte=lister_compte($bdd,$_SESSION['periode'],$preg="^[0-7]") ;

// entete du tableau
echo "<table border=4 cellpading=50 align=center><tr align=center><th>N° Compte</th><th>Nom du Compte</th>
        <th>  Debit  </th><th>  Credit  </th><th>Solde Débiteur</th><th>Solde Créditeur</th></tr>" ;

foreach($listecompte as $compte) //pour chaque compte
    {
    //on récupère les paramètres et ont les affiches dans une ligne
    $parametre=parametre_compte($bdd,$_SESSION['periode'],$compte,0) ;
    if(!($parametre['totaldebit']==0 && $parametre['totalcredit']==0))
        {
        echo    '<tr><td>' 
                . $compte 
                . '</td><td>' 
                . $parametre['label']
                . "</td><td align=right>" 
                . number_format($parametre['totaldebit'], 2,',','') 
                . ' €</td><td align=right>' 
                . number_format($parametre['totalcredit'],2,',','') 
                . ' €</td><td align=right>' ;
        
        // on met a jour les totaux    
        $total_debit+=$parametre['totaldebit'];
        $total_credit+=$parametre['totalcredit'];
        
        // affichage des soldes si positif
    
        if ($parametre['soldedebit'] > 0)
            {
            echo number_format($parametre['soldedebit'],2,',','') . ' €';
            $total_solde_debit+= $parametre['soldedebit'] ;
            }
        echo '</td><td align=right>' ;
    
        if ($parametre['soldecredit'] > 0)
            {
            echo number_format($parametre['soldecredit'],2,',','') . ' €';
            $total_solde_credit+=$parametre['soldecredit'] ;
            }
        echo '</td></tr>' ;
        }
    }

//affichage la ligne des totaux

echo "<tr><td></td><td>Totaux</td><td align='right'>" 
    . number_format( $total_debit,2,',','') 
    ." €</td><td align='right'>"  
    . number_format($total_credit,2,',','') 
    . " €</td><td align='right'>" 
    . number_format($total_solde_debit,2,',','')    
    . " €</td><td align='right'>" 
    . number_format($total_solde_credit,2,',','')    
    . " €</td></tr></table></body></html>" ; 

?>


