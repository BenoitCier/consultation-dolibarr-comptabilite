<?php  
// fichier de conf

include("include/include.php"); 
  echo '<h1>' . $titre_du_site . '</h1>' 
        . '<h3>Du ' . $_SESSION['periode'][0]->format('j/m/Y') .' Au '  . $_SESSION['periode'][1]->format('j/m/Y') . '</h3>' ;  

//on verifie les variable du formulaire et on met a jour les variable de session

if (isset($_POST['date_debut']))
{
$_SESSION['periode'][0]=DateTime::createFromFormat('Y-m-d',$_POST['date_debut']);
}


if (isset($_POST['date_fin']))
{
$_SESSION['periode'][1]=DateTime::createFromFormat('Y-m-d',$_POST['date_fin']);
}

//on traite la valeur a ajouter dans bilan.conf

if (isset($_POST['ajouter']))
    {
    $ajouter=(int)$_POST['ajouter'] ;
    if($ajouter!=0)
        {
        $_SESSION['bilan_conf'][]=$ajouter ;
        }
    }

// on traite la valeur a enlever dans bilan.conf


if (isset($_POST['enlever']))
    {
    $enlever=(int)$_POST['enlever'] ;
    if($enlever!=0)
    {
        //si la valeur est dans le tableau
        if (in_array($enlever,$_SESSION['bilan_conf']))
        {
            //on trouve sa clef
            $cle = array_search($enlever,$_SESSION['bilan_conf']) ;
            //on la supprimer
            unset($_SESSION['bilan_conf'][$cle]) ;
           }
    }
} 

// on ecrit bilan_conf dans bilan.conf

$fichier=fopen('fichier/bilan.conf','w') ;

foreach($_SESSION['bilan_conf'] as $key => $value)
{
fwrite($fichier, $value) ;
fwrite($fichier,"\n");
}
fclose($fichier) ;



// on affiche le formulaire
?>


<h2>Configuration</h2>
<h4>Choix des dates d'affichage</h4>
<form method="post" action="conf.php">
<p><div> Date de début </div>
<input type="date" name="date_debut" value=<?php echo $_SESSION['periode'][0]->format('Y-m-d'); ?> />
<div> Date de fin </div>
<input type="date" name="date_fin" value=<?php echo $_SESSION['periode'][1]->format('Y-m-d'); ?> /><br>
<div> Configuration des comptes de tiers à l'actif du bilan (mettre un numéro de compte par ligne) </div>
Liste actuel de compte : <br><br>

<?php

foreach($_SESSION['bilan_conf'] as $key => $value)
    {
    echo $value ;
    echo '<br>' ;
    }
?>

<br>
Compte a ajouter :<input type="text" name="ajouter" /><br>
Compte a supprimer : <input type="text" name="enlever" />
<br><br>
<input type="submit" value="Valider" />
</p>
</form>
</body></html>
